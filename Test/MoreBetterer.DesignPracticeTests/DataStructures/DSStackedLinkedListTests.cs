﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using MoreBetterer.DesignPractice;
namespace MoreBetterer.DesignPracticeTests.DataStructures
{
    public class DSStackedLinkedListTests
    {



        DSStackedLinkedList Tstack4 = new DSStackedLinkedList();

        



        [Fact]
        public void CreateClass()
        {
            //arrange
            //act
            //assert


            var TSet = new DSStackedLinkedList();

            Assert.NotNull(TSet);
           
        }

        [Fact]
        public void TryPushValidString()
        {
            var Tstack2 = new DSStackedLinkedList();



            bool didpush = Tstack2.push("bird");
            bool emptiness = Tstack2.isempty();


            Assert.True(didpush);
            Assert.False(emptiness);


        }

       [Theory]
       [InlineData(2)]
       [InlineData(250)]
       [InlineData(906)]
        public void TryPushingManyIntsReturnSize(int expectedSize)
        {
            //arrange
            var Tstack3 = new DSStackedLinkedList();
            int val = 1;
         
          //  int expectedSize = 4;

            //act
            for(int i=0;i<expectedSize;i++)
            Tstack3.push(val++);
            



            //assert
            Assert.Equal(expectedSize, Tstack3.Size());



        }

        [Fact]
        public void TryPushingStringReturnTopValue()
        {

            //arrange
           // var Tstack4 = new DSStackedLinkedList();
            string expectedstring = "success";
            string dummystring = "fail";

            //act

            Tstack4.push(dummystring);
            Tstack4.push(dummystring);
            Tstack4.push(expectedstring);

            //arrange
            Assert.Equal(expectedstring, Tstack4.top());



        }

        [Fact]
        public void TryPopStringReturnTopValue()
        {
            //arrange;
            var Tstack5 = new DSStackedLinkedList();
            string expectedstring = "success";
            
            string dummystring = "fail";
            int expectedSize = 5;
            //act
            Tstack5.push(dummystring);
            Tstack5.push(dummystring);
            Tstack5.push(dummystring);
            Tstack5.push(dummystring);
            Tstack5.push(expectedstring);
            Tstack5.push(dummystring);
            var didpop  =Tstack5.pop();
           


            //assert

            Assert.True(didpop);
            Assert.Equal(expectedstring, Tstack5.top());
            Assert.Equal(expectedSize, Tstack5.Size());
           


        }

    


    }
}
