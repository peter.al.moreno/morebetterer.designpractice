﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MoreBetterer.DesignPractice
{
   public class DSStackedLinkedList
    {

        private Node Top = new Node();
        private int size=0;

        private class Node
        {
            public Node next;
            public object data;
        }

        public bool push(object f)
        {

            try
            {
                Node NewNode = new Node();
                NewNode.data = f;
                NewNode.next = Top;
                Top = NewNode;
                size++;
            }
            catch(Exception e)
            {
                return false;

            }
            return true;

        }

        public bool isempty()
        {

            if (size == 0)
                return true;
            else
                return false;
        }

        public int Size()
        {
            return size;
        }

        public object top()
        {
            return Top.data;
        }

        public bool pop()
        {
            if (Top == null)
                return false;

            try {
                Top = Top.next;

                size--;
            }
            catch { return false; }

            return true;

        }
    }
}
