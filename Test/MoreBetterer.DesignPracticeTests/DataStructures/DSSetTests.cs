﻿using System;
using System.Linq;
using Xunit;
using MoreBetterer.DesignPractice;
namespace MoreBetterer.DesignPracticeTests.DataStructures
{
    public class DSSetTests
    {
        


        [Fact]
        public void CreateClass()
        {
            //arrange
            //act
            //assert


            var TSet = new DSSet();

            Assert.NotNull(TSet);
        }


        [Fact]
        public void EmptySet()
        {

            DSSet emptyset = new DSSet();

            Assert.True(emptyset.Isempty());

        }

        [Fact]
        public void GrowSet()
        {
            //arrange
            DSSet Set1= new DSSet();

            //act
            Set1.Add(2);

            //assert
            Assert.True(!Set1.Isempty());
            Assert.True(Set1.Size() > 0);

        }

        [Fact]
        public void Contains()
        {
            //arrange
            DSSet Set2 = new DSSet();
            int item = 4;
            //act
            Set2.Add(item);

            //assert
            Assert.True(Set2.Size() == 1);
            Assert.True(Set2.Contains(item));

        }

        [Fact]
        public void Remove()
        {
            //arrange
            DSSet Set3 = new DSSet();
            int item = 5;

            //act
            Set3.Add(2);
            Set3.Add(item);
            Set3.Add(4);
            Set3.Remove(item);
            //assert
            Assert.True(Set3.Size()==2);
            Assert.False(Set3.Contains(item));

            


        }

    }
}
