﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MoreBetterer.DesignPractice
{
    public class DSSet
    {

        object[] data = new object[5];
        int count;

        public DSSet()
        {
            count = 0;
        }

        public bool Isempty()
        {
            if (count <= 0)
                return true;

            return false;
        }

        public bool Contains(object item)
        {

            if (this.Find(item) >= 0)
                return true;


            return false;
        }

        private int Find(object item)
        {
            for (int i = 0; i < count; i++)
            {
                if (data[i].Equals(item))
                {
                    return i;
                }
            }


            return -1;

        }



        public void Add(object item)
        {
            data[count] = item;
            count++;
        }

        public int Size()
        {
            return count;
        }

        public void Remove(object item)
        {
            int index = this.Find(item);

            if (index<0)
            { return; }

            data[index] = data[count-1];
            
            count--;
        }
    }
}
